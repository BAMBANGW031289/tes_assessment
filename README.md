# Shell
1) 	`ps httpd | wc -l`
2) 	`for xx in mig33/inner.folder/*.txt; do mv $xx ${xx%.txt}.dat; done`


# SQL
1) 	Query 1
```SQL
SELECT 
	ANY_VALUE(DAY(date)) as 'day',
	ANY_VALUE(SUM(CASE WHEN score > 0 THEN score ELSE 0 END)) as 'num_pos_scores',
	ANY_VALUE(SUM(CASE WHEN score < 0 THEN score ELSE 0 END)) as 'num_neg_scores'
FROM assessments
WHERE date BETWEEN '2011-03-01' AND '2011-04-30'
GROUP BY day
```
2) 	Query 2
```SQL
SELECT
	DAY(date),
	score
FROM assessments
WHERE score > 0 AND date BETWEEN '2011-01-01' AND '2011-06-30'
```


# NodeJS, Python, Golang, or PHP
```php

function bil_prima($n)
{
	if ($n <= 0) throw new InvalidArgumentException("positive integer required");
	if ($n < 2) return array();

	// 2 is the only "special" prime we care about
	$primes = array(2);

	// for efficiency, we iterate over odd numbers only
	for ($candidate=3; $candidate<$n; $candidate+=2)
	{
		// we use the collection of previously found primes to limit number of iterations

		// there's no need to go beyond the square root when looking for factors
		$upper_bound = ceil(sqrt($candidate));

		// local variable to avoid lookup at each loop iteration
		$len = count($primes);

		// for loop starts at index 1 because we have already excluded even numbers, so we don’t need to test modulo 2
		for ($i=1; $i<$len; $i++)
		{
			$cur_prime = $primes[$i];
			if ($candidate % $cur_prime === 0) continue 2;
			if ($cur_prime >= $upper_bound) break;
		}

		// if we reach here, we've found a new prime! yeah!!
		$primes[] = $candidate;
	}

	return $primes;
}

echo implode(',', bil_prima(5)),"<br/>";
echo implode(',', bil_prima(10));
```


# Javascript
```javascript
const http = require('http'),
      rdc = {}

// generate keys
rdc.generateKeys = function(initial) {
  return new Promise((resolve) => {
    const lsKey = []

    for (let i = 0; i < initial.length; i++) {
      const itemKeys = Object.keys(initial[i])

      itemKeys.forEach((itemKey) => {
        if (!lsKey.includes(itemKey)) {
          lsKey.push(itemKey)
        }
      })
    }

    resolve(lsKey)
  })
}

// generate values
rdc.generateValues = function(initial, keys) {
  return new Promise((resolve) => {
    const lsValues = []

    for (let i = 0; i < initial.length; i++) {
      const itemValues = []

      keys.forEach((itemKey) => {
        if (initial[i][itemKey] !== undefined) {
          itemValues.push(initial[i][itemKey])
        } else {
          itemValues.push(null)
        }
      })

      lsValues.push(itemValues)
    }

    resolve(lsValues)
  })
}

// node server
http.createServer(async(req, res) => {
  const initial = [
          { username: 'ali', hair_color: 'brown', height: 1.2 },
          { username: 'marc', hair_color: 'blue', height: 1.4 },
          { username: 'joe', hair_color: 'brown', height: 1.7 },
          { username: 'zehua', hair_color: 'black', height: 1.8 },
        ],
        keys = await rdc.generateKeys(initial),
        values = await rdc.generateValues(initial, keys)

  res.writeHead(200, { 'Content-Type': 'application/json' })
  res.end(JSON.stringify({
    h: keys,
    d: values,
  }))
}).listen(8080)
```

# Algorithmic
```php

function hitungs($n)
{
//  $n = 4;
    // table[i] will be storing the number of
    // solutions for value i. We need n+1 rows
    // as the table is consturcted in bottom up
    // manner using the base case (n = 0)
    $table = array_fill(0, $n + 1, NULL);

    // Base case (If given value is 0)
    $table[0] = 1;

    // Pick all integer one by one and update
    // the table[] values after the index
    // greater than or equal to n
    for ($i = 1; $i < $n; $i++)
        for ($j = $i; $j <= $n; $j++)
            $table[$j] += $table[$j - $i];

    return $table[$n];
}


//
echo hitungs(4);

```
